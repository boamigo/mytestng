const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const tsProject = ts.createProject('tsconfig.json', {
	typescript: require('typescript') 
});
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const Jasmine = require('jasmine');

gulp.task('clean', (cb) => {
	return del(['src/**/*.js', 'src/**/*.js.map'], cb);
});

gulp.task('compile', ['clean'], () => {
	var tsResult = gulp.src('src/**/*.ts')
		.pipe(sourcemaps.init())
		.pipe(tsProject());
	return tsResult.js
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('src'));
});

gulp.task('browserify', ['compile'], () => {
	return browserify()
		.add('src/app.js')
		.exclude('angular')
		.ignore('angular')
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('build'));
});

gulp.task('content', ['browserify'], () => {
	return gulp.src(['src/**/*.html', 'src/**/*.css', 'src/bundle.js'])
		.pipe(gulp.dest('build'));
});

gulp.task('libs', ['content'], () => {
	return gulp.src([
		'angular/angular.min.js',
		'angular-route/angular-route.min.js'
	],
	{ cwd: 'node_modules/**' })
	.pipe(gulp.dest('build/libs'));
});

gulp.task('retest', ['compile'], () => {
	let jasmine = new Jasmine();
	jasmine.loadConfigFile('jasmine.json');
	jasmine.configureDefaultReporter({ showColors: true });
	jasmine.execute();
});

gulp.task('test', () => {
	let jasmine = new Jasmine();
	jasmine.loadConfigFile('jasmine.json');
	jasmine.configureDefaultReporter({ showColors: true });
	jasmine.execute();
});