import * as jsonServer from 'json-server';
const server = jsonServer.create();
const router = jsonServer.router('src/db.json');

server.use(router);
server.listen(8080, () => {
	console.log('JSON Server is running');
});