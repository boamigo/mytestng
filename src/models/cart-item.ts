export interface CartItem {
	productId: string;
	name: string;
	price: number;
	quantity: number;
	total?: number;
}
