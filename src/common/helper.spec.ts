import { Helper } from './helper';
import { ProductDto } from '../dto/product-dto';

describe('ProductMapper Suite', () => {
	let location: angular.ILocationService;
	let routeParams: angular.route.IRouteParamsService;
	let target: Helper;
	beforeEach(() => {
		location = <any>{};
		routeParams = <any>{};
		target = new Helper(location, routeParams);
	});

	it('getRouteParam', () => {
		routeParams['THE_KEY'] = 'PARAMETER_VALUE_1111';
		let actual = target.getRouteParam('THE_KEY');
		expect(actual).toBe('PARAMETER_VALUE_1111');
	});

	it('gotoProducts', () => {
		location.path = jasmine.createSpy();
		target.gotoProducts();
		expect(location.path).toHaveBeenCalledWith('/products');
	});
});
