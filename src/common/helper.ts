export class Helper {

	static $inject = ['$location', '$routeParams'];
	constructor(
		private $location: angular.ILocationService,
		private $routeParams: angular.route.IRouteParamsService) {
	}

	getRouteParam(key: string): string {
		return this.$routeParams[key];
	}

	gotoProducts(): void {
		this.$location.path('/products');
	}
}
