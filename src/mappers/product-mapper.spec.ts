import { ProductMapper } from './product-mapper';
import { ProductDto } from '../dto/product-dto';

describe('ProductMapper Suite', () => {
	let target: ProductMapper;
	beforeEach(() => {
		target = new ProductMapper();
	})

	it('mapListToModel', () => {
		let dto1: ProductDto = {
			id: '1111',
			name: 'NAME 1111',
			description: 'DESCRIPTION 1111',
			price: 111.111,
			quantity: 1001
		};
		let dto2: ProductDto = {
			id: '2222',
			name: 'NAME 2222',
			description: 'DESCRIPTION 2222',
			price: 222.222,
			quantity: 2002
		};
		let list = [dto1, dto2];
		let actual = target.mapListToModel(list);
		expect(actual).not.toBeNull();
		expect(actual.length).toBe(2);
		expect(actual[0].id).toBe('1111');
		expect(actual[0].name).toBe('NAME 1111');
		expect(actual[0].description).toBe('DESCRIPTION 1111');
		expect(actual[0].price).toBe(111.111);
		expect(actual[0].quantity).toBe(1001);
		expect(actual[1].id).toBe('2222');
		expect(actual[1].name).toBe('NAME 2222');
		expect(actual[1].description).toBe('DESCRIPTION 2222');
		expect(actual[1].price).toBe(222.222);
		expect(actual[1].quantity).toBe(2002);
	});

	it('mapToModel', () => {
		let dto: ProductDto = {
			id: '1111',
			name: 'NAME 1111',
			description: 'DESCRIPTION 1111',
			price: 111.111,
			quantity: 1001
		};
		let actual = target.mapToModel(dto);
		expect(actual).not.toBeNull();
		expect(actual.id).toBe('1111');
		expect(actual.name).toBe('NAME 1111');
		expect(actual.description).toBe('DESCRIPTION 1111');
		expect(actual.price).toBe(111.111);
		expect(actual.quantity).toBe(1001);
	});
});
