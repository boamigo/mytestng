import { CartItem } from "../models/cart-item";
import { ProductModel } from "../models/product-model";

export class CartMapper {
	ensureCartDefined(cartItems: CartItem[]): CartItem[] {
		if (!cartItems) {
			cartItems = [];
		}

		return cartItems;
	}

	mapProductToCartItems(model: ProductModel, cartItems: CartItem[]): void {
		let cartItem = cartItems.find(o => o.productId === model.id);
		if (cartItem) {
			cartItem.quantity ++;
		}
		else {
			cartItems.push({
				productId: model.id,
				name: model.name,
				price: model.price,
				quantity: 1
			});
		}
	}
}
