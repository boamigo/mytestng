import { CartMapper } from './cart-mapper';
import { ProductDto } from '../dto/product-dto';
import { CartItem } from '../models/cart-item';
import { ProductModel } from '../models/product-model';

describe('CartMapper Suite', () => {
	let target: CartMapper;
	beforeEach(() => {
		target = new CartMapper();
	})

	it('ensureCartDefined defined', () => {
		let items: CartItem[] = [<any>{}, <any>{}];
		let actual = target.ensureCartDefined(items);
		expect(actual).toBe(items);
	});

	it('ensureCartDefined not defined', () => {
		let actual = target.ensureCartDefined(null);
		expect(actual).not.toBeNull();
		expect(actual.length).toBe(0);
	});

	it('mapProductToCartItems new', () => {
		let product: ProductModel = {
			id: '1111',
			name: 'NAME',
			description: 'DESC',
			price: 11.22,
			quantity: 500
		};
		let items: CartItem[] = [<any>{ productId: '2222', quantity: 1 }, <any>{ productId: '3333', quantity: 1 }];
		target.mapProductToCartItems(product, items);
		expect(items.length).toBe(3);
		expect(items[2].productId).toBe('1111');
		expect(items[2].name).toBe('NAME');
		expect(items[2].price).toBe(11.22);
		expect(items[2].quantity).toBe(1);
	});

	it('mapProductToCartItems existing', () => {
		let product: ProductModel = {
			id: '3333',
			name: 'NAME',
			description: 'DESC',
			price: 11.22,
			quantity: 500
		};
		let items: CartItem[] = [<any>{ productId: '2222', quantity: 1 }, <any>{ productId: '3333', quantity: 2 }];
		target.mapProductToCartItems(product, items);
		expect(items.length).toBe(2);
		expect(items[1].productId).toBe('3333');
		expect(items[1].quantity).toBe(3);
	});
});
