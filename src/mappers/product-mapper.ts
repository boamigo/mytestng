import { ProductModel } from '../models/product-model';
import { ProductDto } from '../dto/product-dto';

export class ProductMapper {
	mapListToModel(dtoList: ProductDto[]): ProductModel[] {
		let models: ProductModel[] = [];
		for(let dto of dtoList) {
			models.push({
				id: dto.id,
				name: dto.name,
				description: dto.description,
				price: dto.price,
				quantity: dto.quantity
			});
		}

		return models;
	}

	mapToModel(dto: ProductDto): ProductModel {
		return {
			id: dto.id,
			name: dto.name,
			description: dto.description,
			price: dto.price,
			quantity: dto.quantity
		};
	}
}
