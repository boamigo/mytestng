import * as angular from 'angular';

import { Rest } from './services/helpers/rest';
import { TypedStorage } from './services/helpers/typed-storage';
import { ProductService } from './services/product-service';
import { CartService } from './services/cart-service';

import { Helper } from './common/helper';
import { ProductMapper } from './mappers/product-mapper';
import { CartMapper } from './mappers/cart-mapper';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { CartComponent } from './components/cart/cart.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';

import { IdTextFilter } from './filters/idtext-filter';
import { PriceFilter } from './filters/price-filter';

export class Application {
	private static appModule : angular.IModule;
	public static create() {
		Application.appModule = angular.module('mytestng', ['ngRoute']);
		Application.appModule.config(['$routeProvider', '$locationProvider', ($routeProvider: angular.route.IRouteProvider, $locationProvider: angular.ILocationProvider) => {
			$locationProvider.html5Mode({ enabled:true });
			$routeProvider
			.when('/', {
				template: '<home></home>'
			})
			.when('/products', {
				template: '<product-list></product-list>'
			})
			.when('/products/:productId', {
				template: '<product-view></product-view>'
			})
			.when('/cart', {
				template: '<cart></cart>'
			});
		}]);
	}

	public static service(name: string, serviceConstructor: angular.Injectable<Function>) {
		Application.appModule.service(name, serviceConstructor);
	}

	public static controller(name: string, controllerConstructor: angular.Injectable<angular.IControllerConstructor>) {
		Application.appModule.controller(name, controllerConstructor);
	}

	public static filter(name: string, filterFactoryFunction: angular.Injectable<Function>) {
		Application.appModule.filter(name, filterFactoryFunction);
	}

	public static directive(name: string, directiveConstructor: angular.Injectable<any>) {
		Application.appModule.directive(name, directiveConstructor);
	}

	public static component(name: string, options: angular.IComponentOptions) {
		Application.appModule.component(name, options);
	}
}

(():void => {
	Application.create();
	Application.service('Rest', Rest);
	Application.service('CartMapper', CartMapper);
	Application.service('TypedStorage', TypedStorage);
	Application.service('ProductService', ProductService);
	Application.service('CartService', CartService);

	Application.service('Helper', Helper);
	Application.service('ProductMapper', ProductMapper);
	Application.component('productList', { controller: ProductListComponent, templateUrl: ProductListComponent.TemplateUrl });
	Application.component('productView', { controller: ProductViewComponent, templateUrl: ProductViewComponent.TemplateUrl });
	Application.component('cart', { controller: CartComponent, templateUrl: CartComponent.TemplateUrl });
	Application.component('navbar', { controller: NavbarComponent, templateUrl: NavbarComponent.TemplateUrl });
	Application.component('home', { controller: HomeComponent, templateUrl: HomeComponent.TemplateUrl });

	Application.filter("idtext", IdTextFilter.factory);
	Application.filter("price", PriceFilter.factory);
})();
