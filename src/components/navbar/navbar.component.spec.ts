import { ICartListener, CartService } from '../../services/cart-service';
import { NavbarComponent } from './navbar.component';

describe('Rest Suite', () => {
	let scope: angular.IScope;
	let cartService: CartService;
	let target: NavbarComponent;
	beforeEach(() => {
		scope = <any>{};
		cartService = <any>{};
		target = new NavbarComponent(scope, cartService);
	});

	it('$onInit', () => {
		cartService.subscribeCartChange = jasmine.createSpy();
		cartService.getItems = jasmine.createSpy().and.returnValue([{}, {}, {}]);
		target.$onInit();
		expect(target.count).toBe(3);
		expect(cartService.subscribeCartChange).toHaveBeenCalledWith(scope, target);
	});

	it('onCartUpdated', () => {
		cartService.getItems = jasmine.createSpy().and.returnValue([{}, {}, {}]);
		target.onCartUpdated();
		expect(target.count).toBe(3);
	});
});
