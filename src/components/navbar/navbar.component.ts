import { ICartListener, CartService } from '../../services/cart-service';

export class NavbarComponent implements angular.IComponentController, ICartListener {
	static TemplateUrl = '/components/navbar/navbar.component.html';
	count: number;

	static $inject = ['$scope', 'CartService'];
	constructor (
		private $scope: angular.IScope,
		private cartService: CartService) {
	}

	$onInit(): void {
		this.cartService.subscribeCartChange(this.$scope, this);
		this.count = this.cartService.getItems().length;
	}

	onCartUpdated(): void {
		this.count = this.cartService.getItems().length;
	}
}
