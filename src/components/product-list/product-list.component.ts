import { ProductModel } from '../../models/product-model';
import { ProductMapper } from '../../mappers/product-mapper';
import { ProductService } from '../../services/product-service';

export class ProductListComponent implements angular.IComponentController {
	static TemplateUrl = '/components/product-list/product-list.component.html';
	products: ProductModel[] = null;

	static $inject = ['$scope', 'ProductService', 'ProductMapper'];
	constructor (
		private $scope: angular.IScope,
		private productService: ProductService,
		private productMapper: ProductMapper) {
	}

	$onInit(): void {
		this.productService.getAll()
			.then(data => this.products = this.productMapper.mapListToModel(data));
	}
}
