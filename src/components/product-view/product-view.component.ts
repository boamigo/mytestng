import { ProductModel } from '../../models/product-model';
import { ProductDto } from '../../dto/product-dto';
import { Helper } from '../../common/helper';
import { ProductMapper } from '../../mappers/product-mapper';
import { ProductService } from '../../services/product-service';
import { CartService } from '../../services/cart-service';

export class ProductViewComponent implements angular.IComponentController {
	static TemplateUrl = '/components/product-view/product-view.component.html';
	productId: string;
	product: ProductModel;

	static $inject = ['Helper', 'ProductService', 'ProductMapper', 'CartService'];
	constructor (
		private helper: Helper,
		private productService: ProductService,
		private productMapper: ProductMapper,
		private cartService: CartService) {
	}

	$onInit(): angular.IPromise<any> {
		this.productId = this.helper.getRouteParam('productId');
		return this.productService.getById(this.productId)
			.then(data => this.product = this.productMapper.mapToModel(data));
	}

	backToProducts(): void {
		this.helper.gotoProducts();
	}

	addToCart(): void {
		this.cartService.addItem(this.product);
		this.cartService.notifyCartUpdated();
	}
}
