import { ProductViewComponent } from './product-view.component';
import { Helper } from '../../common/helper';
import { ProductMapper } from '../../mappers/product-mapper';
import { ProductService } from '../../services/product-service';
import { CartService } from '../../services/cart-service';
import { ProductModel } from '../../models/product-model';
import { ProductDto } from '../../dto/product-dto';

describe('Rest Suite', () => {
	let helper: Helper;
	let productService: ProductService;
	let productMapper: ProductMapper;
	let cartService: CartService;
	let target: ProductViewComponent;
	beforeEach(() => {
		helper = <any>{};
		productService = <any>{};
		productMapper = <any>{};
		cartService = <any>{};
		target = new ProductViewComponent(helper, productService, productMapper, cartService);
	});

	it('$onInit', async () => {
		let dto: ProductDto = <any>{ id: 1111 };
		let model: ProductModel = <any>{ id: 2222 };
		helper.getRouteParam = jasmine.createSpy().and.returnValue('PRODUCT_ID_1111');
		productService.getById = jasmine.createSpy().and.returnValue(Promise.resolve(dto));
		productMapper.mapToModel = jasmine.createSpy().and.returnValue(model);
		await target.$onInit();
		expect(target.productId).toBe('PRODUCT_ID_1111');
		expect(target.product).toBe(model);
	});

	it('backToProducts', () => {
		helper.gotoProducts = jasmine.createSpy();
		target.backToProducts();
		expect(helper.gotoProducts).toHaveBeenCalled();
	});

	it('addToCart', () => {
		cartService.addItem = jasmine.createSpy();
		cartService.notifyCartUpdated = jasmine.createSpy();
		let product: ProductModel = <any>{ id: 7777 };
		target.product = product;
		target.addToCart();
		expect(cartService.addItem).toHaveBeenCalledWith(product);
		expect(cartService.notifyCartUpdated).toHaveBeenCalled();
	});
});
