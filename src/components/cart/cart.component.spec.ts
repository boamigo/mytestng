import { ICartListener, CartService } from '../../services/cart-service';
import { CartComponent } from './cart.component';
import { CartItem } from '../../models/cart-item';

describe('Rest Suite', () => {
	let cartService: CartService;
	let target: CartComponent;
	beforeEach(() => {
		cartService = <any>{};
		target = new CartComponent(cartService);
	});

	it('$onInit', () => {
		let items: CartItem[] = [ <any>{}, <any>{}, <any>{}];
		cartService.getItems = jasmine.createSpy().and.returnValue(items);
		cartService.getTotalAndRecalculate = jasmine.createSpy().and.returnValue(1111);
		target.$onInit();
		expect(target.items).toBe(items);
		expect(target.total).toBe(1111);
	});

	it('clearAll', () => {
		let items: CartItem[] = [ <any>{}, <any>{}, <any>{}];
		cartService.clear = jasmine.createSpy();
		cartService.notifyCartUpdated = jasmine.createSpy();
		cartService.getItems = jasmine.createSpy().and.returnValue(items);
		cartService.getTotalAndRecalculate = jasmine.createSpy().and.returnValue(1111);
		target.clearAll();
		expect(target.items).toBe(items);
		expect(target.total).toBe(1111);
	});
});
