import { CartService } from '../../services/cart-service';
import { CartItem } from '../../models/cart-item';

export class CartComponent implements angular.IComponentController {
	static TemplateUrl = '/components/cart/cart.component.html';
	items: CartItem[] = null;
	total: number;

	static $inject = ['CartService'];
	constructor (
		private cartService: CartService) {
	}

	$onInit(): void {
		this.items = this.cartService.getItems();
		this.total = this.cartService.getTotalAndRecalculate(this.items);
	}

	clearAll(): void {
		this.cartService.clear();
		this.cartService.notifyCartUpdated();
		this.items = this.cartService.getItems();
		this.total = this.cartService.getTotalAndRecalculate(this.items);
	}
}
