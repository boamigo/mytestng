import { Rest } from './helpers/rest';
import { ProductService } from './product-service';

describe('ProductService Suite', () => {
	let rest: Rest;
	let target: ProductService;
	beforeEach(() => {
		rest = <any>{};
		target = new ProductService(rest);
	});

	it('getAll', async () => {
		rest.get = jasmine.createSpy();
		let actual = await target.getAll();
		expect(rest.get).toHaveBeenCalledWith('/products');
	});

	it('getById', async () => {
		rest.get = jasmine.createSpy();
		let actual = await target.getById('1111');
		expect(rest.get).toHaveBeenCalledWith('/products/1111');
	});
});
