export class TypedStorage {
	getData<T>(key: string): T {
		let text = localStorage.getItem(key);
		if (!text) {
			return null;
		}

		return JSON.parse(text);
	}

	setData<T>(key: string, data: T): void {
		let text = JSON.stringify(data);
		localStorage.setItem(key, text);
	}

	clearData(key: string): void {
		localStorage.removeItem(key);
	}
}
