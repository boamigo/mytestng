import { Rest } from './rest';

describe('Rest Suite', () => {
	let http: angular.IHttpService;
	let target: Rest;
	beforeEach(() => {
		http = <any>{};
		target = new Rest(http)
	})

	it('get', async () => {
		let url = 'THE URL';
		let expected = { id: 100, name: '200'};
		http.get = jasmine.createSpy().and.returnValue(Promise.resolve({ data: expected}));
		let actual = await target.get<any>(url);
		expect(actual).toBe(expected);
	});
});
