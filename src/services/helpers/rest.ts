import * as angular from 'angular';

export class Rest {
	static $inject = ['$http'];
	constructor(
		private $http: angular.IHttpService) {
	}

	get<T>(url: string): angular.IPromise<T> {
		return this.$http.get(url)
			.then(response => <T>response.data);
	}
}
