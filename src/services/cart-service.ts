import { ProductModel } from '../models/product-model';
import { CartItem } from '../models/cart-item';
import { TypedStorage } from './helpers/typed-storage';
import { CartMapper } from '../mappers/cart-mapper';

export interface ICartListener {
	onCartUpdated();
}

const CART_KEY = 'CART';
export class CartService {

	static $inject = ['TypedStorage', 'CartMapper', '$rootScope'];
	constructor(
		private typedStorage: TypedStorage,
		private cartMapper: CartMapper,
		private $rootScope: angular.IRootScopeService) {
	}

	addItem(product: ProductModel): void {
		let cartItems: CartItem[] = this.typedStorage.getData(CART_KEY);
		cartItems = this.cartMapper.ensureCartDefined(cartItems);
		this.cartMapper.mapProductToCartItems(product, cartItems);
		this.typedStorage.setData(CART_KEY, cartItems);
	}

	getItems(): CartItem[] {
		let cartItems: CartItem[] = this.typedStorage.getData(CART_KEY);
		return this.cartMapper.ensureCartDefined(cartItems);
	}

	getTotalAndRecalculate(items: CartItem[]): number {
		let total = 0.0;
		for(let item of items) {
			item.total = item.price * item.quantity;
			total += item.total;
		}

		return total;
	}

	subscribeCartChange(scope: angular.IScope, listener: ICartListener): void {
		let handler = this.$rootScope.$on('cart-updated', () => listener.onCartUpdated());
		scope.$on('$destroy', handler);
	}

	notifyCartUpdated(): void {
		this.$rootScope.$emit('cart-updated');
	}

	clear(): void {
		this.typedStorage.clearData(CART_KEY);
	}
}
