import { CartService, ICartListener } from './cart-service';
import { ProductModel } from '../models/product-model';
import { CartItem } from '../models/cart-item';
import { TypedStorage } from './helpers/typed-storage';
import { CartMapper } from '../mappers/cart-mapper';

describe('CartService Suite', () => {
	let typedStorage: TypedStorage;
	let cartMapper: CartMapper;
	let rootScope: angular.IRootScopeService;
	let target: CartService;
	beforeEach(() => {
		typedStorage = <any>{};
		cartMapper = <any>{};
		rootScope = <any>{};
		target = new CartService(typedStorage, cartMapper, rootScope);
	});

	it('addItem', () => {
		let items: CartItem[] = [<any>{}, <any>{}, <any>{}];
		let ensuredItems: CartItem[] = [<any>{}, <any>{}, <any>{}];
		let product: ProductModel = <any>{ id: 1111 };
		typedStorage.getData = jasmine.createSpy().and.returnValue(items);
		cartMapper.ensureCartDefined = jasmine.createSpy().and.returnValue(ensuredItems);
		cartMapper.mapProductToCartItems = jasmine.createSpy();
		typedStorage.setData = jasmine.createSpy();

		target.addItem(product);
		expect(typedStorage.getData).toHaveBeenCalledWith('CART');
		expect(cartMapper.ensureCartDefined).toHaveBeenCalledWith(items);
		expect(cartMapper.mapProductToCartItems).toHaveBeenCalledWith(product, ensuredItems);
		expect(typedStorage.setData).toHaveBeenCalledWith('CART', ensuredItems);
	});

	it('subscribeCartChange', () => {
		let handler = () => {};
		let scope: angular.IScope = <any>{};
		scope.$on = jasmine.createSpy();
		rootScope.$on = jasmine.createSpy().and.returnValue(handler);
		let listener: ICartListener = <any>{};
		target.subscribeCartChange(scope, listener);
		expect(rootScope.$on).toHaveBeenCalledWith('cart-updated', jasmine.anything());
		expect(scope.$on).toHaveBeenCalledWith('$destroy', handler);
	});
});
