import { Rest } from './helpers/rest';
import { ProductDto } from '../dto/product-dto';

export class ProductService {

	static $inject = ['Rest'];
	constructor(private rest: Rest) {
	}

	getAll(): angular.IPromise<ProductDto[]> {
		return this.rest.get('/products');
	}

	getById(id: string): angular.IPromise<ProductDto> {
		return this.rest.get('/products/' + id);
	}
}
