import { PriceFilter } from './price-filter';

describe('PriceFilter Suite', () => {
	let target: (value: number) => string;
	beforeEach(() => {
		target = PriceFilter.factory();
	});

	it('null', () => {
		let actual = target(null);
		expect(actual).toBeNull();
	});

	it('valid 1', () => {
		let actual = target(111.222);
		expect(actual).toBe('$111.22');
	});

	it('valid 2', () => {
		let actual = target(111.888);
		expect(actual).toBe('$111.89');
	});
});
