import { IdTextFilter } from './idtext-filter';

describe('IdTextFilter Suite', () => {
	let target: (value: string) => string;
	beforeEach(() => {
		target = IdTextFilter.factory();
	});

	it('null', () => {
		let actual = target(null);
		expect(actual).toBeNull();
	});

	it('valid 1', () => {
		let actual = target('123');
		expect(actual).toBe('000123');
	});

	it('valid 2', () => {
		let actual = target('123456');
		expect(actual).toBe('123456');
	});

	it('overflow', () => {
		let actual = target('1234567');
		expect(actual).toBe('234567');
	});
});
