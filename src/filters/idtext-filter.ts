export class IdTextFilter {
	static $inject = [];
	static factory(): any {
		return (input: string): string =>
		{
			if (!input) {
				return null;
			}


			let text = '000000' + input;
			text = text.slice(-6);
			return text;
		}
	}
}
