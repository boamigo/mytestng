export class PriceFilter {
	static $inject = [];
	static factory(): any {
		return (value: number): string =>
		{
			if (!value) {
				return null;
			}
	
	
			value = Math.round(value * 100) / 100;
			return '$' + value;
		}
	}
}