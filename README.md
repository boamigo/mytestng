Before running install:
    gulp            npm install gulp-cli -g
    jasmine         npm install -g jasmine
    dependencies    npm install

Commands:
    npm run build   compile and build sources
    npm run test    compile and run unit tests
    npm run start   start server at localhost:3000
